// Enlace a la aplicación express
const app = require('./app/app');

// Listening port
const port = process.env.PORT || 3000;

// Escucha permanente en el puerto anterior
app.listen(port, () => {
  console.log(`Service listening at port ${port}`);
});