// Inicialización de la app express
const express = require('express');
const app = express();

// Variables globales para almacenar datos
var usuario = 'francisco';
var correo = 'fjsuarez@uniovi.es';

// ruta '/'
app.get('/', (req, res) => {
  res.send('Wellcome to node server');
});

// ruta '/about'
app.get('/about', (req, res) => {
  res.sendFile(__dirname+'/views/about.html');
});

// API REST

// middleware for applying CORS only to expose our entire API
app.use('/api', require('cors')());

// middleware to support JSON-encoded bodies
app.use(express.json());       

// ruta '/api/datos' -> tratamiento de petición GET (enviada por el cliente para recibir los datos)
app.get('/api/datos', function(req, res){
  // envío de los datos
  res.json({username: usuario, email: correo});
});

module.exports = app;